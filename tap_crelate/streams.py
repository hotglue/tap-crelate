"""Stream type classes for tap-crelate."""

from __future__ import annotations

import sys
import typing as t

from singer_sdk import typing as th  # JSON Schema typing helpers

from tap_crelate.client import crelateStream

if sys.version_info >= (3, 9):
    import importlib.resources as importlib_resources
else:
    import importlib_resources


class ActivitiesStream(crelateStream):
    """Define custom stream."""

    name = "activities"
    path = "/activities"
    #replication_key = "ModifiedOn"
    schema = th.PropertiesList(
        th.Property("AllDay", th.BooleanType),
        th.Property("Announced", th.BooleanType),
        th.Property("AttachmentId", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType),
            th.Property("EntityName", th.StringType))),

        th.Property("Attendees", th.StringType),
        th.Property("BCC", th.StringType),
        th.Property("CC", th.StringType),
        th.Property("Completed", th.BooleanType),
        th.Property("CompletedOn", th.DateTimeType),
        th.Property("Display", th.StringType),
        th.Property("DocumentTypeId", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("DueDate", th.DateTimeType),
        th.Property("FormId", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("From", th.StringType),
        th.Property("Id", th.UUIDType),
        th.Property("IsCallCompleted", th.BooleanType),
        th.Property("IsEngagement", th.BooleanType),
        th.Property("IsInvite", th.BooleanType),
        th.Property("IsReachOut", th.BooleanType),
        th.Property("Location", th.StringType),
        th.Property("ModifiedOn", th.DateTimeType), 
        th.Property("Owners", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType),
            th.Property("IsPrimary", th.BooleanType))),

        th.Property("ParentExperienceId", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("ParentId", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType),
            th.Property("EntityName", th.StringType))),

        th.Property("Pinned", th.BooleanType),
        th.Property("Rating", th.StringType),
        th.Property("RegardingId", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("Subject", th.StringType),
        th.Property("To", th.StringType),
        th.Property("VerbId", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("WhatEntityIds", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType),
            th.Property("EntityName", th.StringType))),

        th.Property("When", th.DateTimeType),
        th.Property("WhenEnd", th.DateTimeType),
        th.Property("WhenOffset", th.IntegerType),
        th.Property("WhoAttendeeIds", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType),
            th.Property("EntityName", th.StringType))),

        th.Property("WorkFlowItemId", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
    ).to_dict()


class TasksStream(crelateStream):
    """Define custom stream."""

    name = "tasks"
    path = "/tasks"
    #replication_key = "ModifiedOn"
    schema = th.PropertiesList(
        th.Property("Announced", th.BooleanType),
        th.Property("CreatedBy", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("CreatedOn", th.DateTimeType),
        th.Property("CreatedOnSystem", th.DateTimeType),
        th.Property("Display", th.StringType),
        th.Property("ExeternalPrimaryKey", th.StringType),
        th.Property("Id", th.UUIDType),
        th.Property("ModifiedOn", th.DateTimeType),
        th.Property("Owners", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType),
            th.Property("IsPrimary", th.BooleanType))),

        th.Property("ParentId", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType),
            th.Property("EntityName", th.StringType))),

        th.Property("Pinned", th.BooleanType),
        th.Property("RegardingId", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("When", th.DateTimeType),
    ).to_dict()

class UsersStream(crelateStream):
    """Define custom stream."""

    name = "users"
    path = "/users"
    #replication_key = "ModifiedOn"
    schema = th.PropertiesList(
        th.Property("Color", th.StringType),
        th.Property("ContactId", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("CreatedOn", th.DateTimeType),
        th.Property("Email", th.StringType),
        th.Property("FirstName", th.StringType),
        th.Property("FullName", th.StringType),
        th.Property("IconAttachmentId", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("Id", th.UUIDType),
        th.Property("Initials", th.StringType),
        th.Property("LastName", th.StringType),
        th.Property("ModifiedOn", th.StringType),
        th.Property("UserStateId", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("UserTypeId", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
    ).to_dict()

class PaymentsStream(crelateStream):
    name = "payments"
    path = "/payments"
    #replication_key = "ModifiedOn"
    schema = th.PropertiesList(
        th.Property("AccountId", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("Amount", th.NumberType),
        th.Property("CreatedById", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("CreatedOn", th.DateTimeType),
        th.Property("ExternalId", th.StringType),
        th.Property("Id", th.UUIDType),
        th.Property("ModifiedOn", th.DateTimeType),
        th.Property("PaymentMethodOption", th.IntegerType),
        th.Property("PaymentNum", th.IntegerType),
        th.Property("PaymentTypeOption", th.IntegerType),
        th.Property("ReceivedOn", th.DateTimeType),
    ).to_dict()

class InvoicesStream(crelateStream):
    name = "invoices"
    path = "/invoices"
    #replication_key = "ModifiedOn"
    schema = th.PropertiesList(
        th.Property("AccountId", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("Balance", th.IntegerType),
        th.Property("BalanceChangedOn", th.DateTimeType),
        th.Property("CreatedById", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("CreatedOn", th.DateTimeType),
        th.Property("Credits", th.IntegerType),
        th.Property("Discounts", th.IntegerType),
        th.Property("DueOn", th.DateTimeType),
        th.Property("EmployeeId", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("EntitiyStatus", th.IntegerType),
        th.Property("ExternalInvoiceId", th.StringType),
        th.Property("ExternalInvoiceUrl", th.StringType),
        th.Property("FirstSentById", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),  
        
        th.Property("FirstSentOn", th.DateTimeType),
        th.Property("HasError", th.BooleanType),
        th.Property("Id", th.UUIDType),
        th.Property("InvoiceDate", th.DateTimeType),
        th.Property("InvoiceNumber", th.IntegerType),
        th.Property("IsLate", th.BooleanType),
        th.Property("IsSentToExternal", th.BooleanType),
        th.Property("JobId", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        
        th.Property("LastSentById", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("LastSentOn", th.DateTimeType),
        th.Property("ModifiedOn", th.DateTimeType),
        th.Property("Name", th.StringType),
        th.Property("Payments", th.IntegerType),
        th.Property("PrimaryDocumentAttachmentId", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("SendToExternalById", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("SendToExternalOn", th.DateTimeType),
        th.Property("Subtotal", th.IntegerType),
        th.Property("Taxes", th.IntegerType),
        th.Property("TermsOption", th.IntegerType),
        th.Property("Total", th.IntegerType),
        th.Property("VoidDate", th.DateTimeType),
        th.Property("VoidedById", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("VoidedOn", th.DateTimeType),
        th.Property("WasSent", th.BooleanType),
        th.Property("WriteOffs", th.IntegerType),
    ).to_dict()

class PlacementsStream(crelateStream):
    name = "placements"
    path = "/placements"
    #replication_key = "ModifiedOn"
    schema = th.PropertiesList(
        th.Property("AccountId", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("AdditionalDetails", th.StringType),
        th.Property("Addresses_Business", th.PropertiesList(
            th.Property("City", th.StringType),
            th.Property("CountryId", th.PropertiesList(
                th.Property("Id", th.UUIDType),
                th.Property("Title", th.StringType))),

            th.Property("CreatedOnSystem", th.DateTimeType),
            th.Property("Id", th.UUIDType),
            th.Property("IsPrimary", th.BooleanType),
            th.Property("Line1", th.StringType),
            th.Property("Line2", th.StringType),
            th.Property("Location", th.PropertiesList(
                th.Property("Lat", th.NumberType),
                th.Property("Lon", th.NumberType))),
                
            th.Property("State", th.StringType),
            th.Property("ZipCode", th.StringType))),

        th.Property("BillDate", th.DateTimeType),
        th.Property("BillRate", th.NumberType),
        th.Property("Bonus", th.NumberType),
        th.Property("BurdenRate", th.NumberType),
        th.Property("BurdenRatePercentage", th.NumberType),
        th.Property("candidateSourceId", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("CreatedOn", th.DateTimeType),
        th.Property("CustomField1", th.StringType),
        th.Property("CustomField10", th.StringType),
        th.Property("CustomField11", th.NumberType),
        th.Property("CustomField12", th.NumberType),
        th.Property("CustomField13", th.NumberType),
        th.Property("CustomField14", th.NumberType),
        th.Property("CustomField15", th.NumberType),
        th.Property("CustomField16", th.NumberType),
        th.Property("CustomField17", th.NumberType),
        th.Property("CustomField18", th.NumberType),
        th.Property("CustomField19", th.NumberType),
        th.Property("CustomField2", th.StringType),
        th.Property("CustomField20", th.NumberType),
        th.Property("CustomField21", th.DateTimeType),
        th.Property("CustomField22", th.DateTimeType),
        th.Property("CustomField23", th.DateTimeType),
        th.Property("CustomField24", th.DateTimeType),
        th.Property("CustomField25", th.DateTimeType),
        th.Property("CustomField26", th.DateTimeType),
        th.Property("CustomField27", th.DateTimeType),
        th.Property("CustomField28", th.DateTimeType),
        th.Property("CustomField29", th.DateTimeType),
        th.Property("CustomField3", th.StringType),
        th.Property("CustomField30", th.DateTimeType),
        th.Property("CustomField31", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField32", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField33", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField34", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField35", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField36", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField37", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField38", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField39", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField4", th.StringType),
        th.Property("CustomField40", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField41", th.StringType),
        th.Property("CustomField42", th.StringType),
        th.Property("CustomField43", th.StringType),
        th.Property("CustomField44", th.StringType),
        th.Property("CustomField45", th.StringType),
        th.Property("CustomField46", th.StringType),
        th.Property("CustomField47", th.StringType),
        th.Property("CustomField48", th.StringType),
        th.Property("CustomField49", th.StringType),
        th.Property("CustomField5", th.NumberType),
        th.Property("CustomField50", th.StringType),
        th.Property("CustomField51", th.NumberType),
        th.Property("CustomField52", th.NumberType),
        th.Property("CustomField53", th.NumberType),
        th.Property("CustomField54", th.NumberType),
        th.Property("CustomField55", th.NumberType),
        th.Property("CustomField56", th.NumberType),
        th.Property("CustomField57", th.NumberType),
        th.Property("CustomField58", th.NumberType),
        th.Property("CustomField59", th.NumberType),
        th.Property("CustomField6", th.StringType),
        th.Property("CustomField60", th.NumberType),
        th.Property("CustomField61", th.DateTimeType),
        th.Property("CustomField62", th.DateTimeType),
        th.Property("CustomField63", th.DateTimeType),
        th.Property("CustomField64", th.DateTimeType),
        th.Property("CustomField65", th.DateTimeType),
        th.Property("CustomField66", th.DateTimeType),
        th.Property("CustomField67", th.DateTimeType),
        th.Property("CustomField68", th.DateTimeType),
        th.Property("CustomField69", th.DateTimeType),
        th.Property("CustomField7", th.StringType),
        th.Property("CustomField70", th.DateTimeType),
        th.Property("CustomField71", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField72", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField73", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField74", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField75", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField76", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField77", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField78", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField79", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField8", th.StringType),
        th.Property("CustomField80", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField9", th.StringType),
        th.Property("Discount", th.NumberType),
        th.Property("Duration", th.NumberType),
        th.Property("EndDate", th.DateTimeType),
        th.Property("EndingReason", th.IntegerType),
        th.Property("EndingReasonDescription", th.StringType),
        th.Property("EntitiyStatus", th.IntegerType),
        th.Property("EstimatedEndDate", th.DateTimeType),
        th.Property("ExperienceId", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("Fee", th.NumberType),
        th.Property("FeePercent", th.NumberType),
        th.Property("HourlyRate", th.NumberType),
        th.Property("Id", th.UUIDType),
        th.Property("JobOpenDate", th.DateTimeType),
        th.Property("JobSchedule", th.StringType),
        th.Property("JobTtleId", th.PropertiesList(
                th.Property("Id", th.UUIDType),
                th.Property("Title", th.StringType))),

        th.Property("LeadSourceId", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("Margin", th.NumberType),
        th.Property("ModifiedOn", th.DateTimeType),
        th.Property("Name", th.StringType),
        th.Property("OpprotunityTypeId", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("OTBillRate", th.NumberType),
        th.Property("OTBillRateMultiplier", th.NumberType),
        th.Property("OTPayRate", th.NumberType),
        th.Property("OTPayRateMultiplier", th.NumberType),
        th.Property("Owners", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType),
            th.Property("IsPrimary", th.BooleanType))),

        th.Property("PayRate", th.NumberType),
        th.Property("PayRateType", th.NumberType),
        th.Property("PlacementNum", th.IntegerType),
        th.Property("Salary", th.NumberType),
        th.Property("Shift", th.IntegerType),
        th.Property("StartDate", th.DateTimeType),
        th.Property("StatusReason", th.StringType),
        th.Property("TimeToFill", th.NumberType),
        th.Property("TotalHours", th.NumberType),
        th.Property("UserId", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("value", th.NumberType),
        th.Property("VerbId", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("WarrantyDate", th.DateTimeType),
        th.Property("When", th.DateTimeType),
    ).to_dict()

class ApplicationsStream(crelateStream):
    name = "applications"
    path = "/applications"
    #replication_key = "ModifiedOn"
    schema = th.PropertiesList(
        th.Property("Addresses_Business", th.PropertiesList(
            th.Property("City", th.StringType),
            th.Property("CountryId", th.PropertiesList(
                th.Property("Id", th.UUIDType),
                th.Property("Title", th.StringType))),

            th.Property("CreatedOnSystem", th.DateTimeType),
            th.Property("Id", th.UUIDType),
            th.Property("IsPrimary", th.BooleanType),
            th.Property("Line1", th.StringType),
            th.Property("Line2", th.StringType),
            th.Property("Location", th.PropertiesList(
                th.Property("Lat", th.NumberType),
                th.Property("Lon", th.NumberType))),

            th.Property("State", th.StringType),
            th.Property("ZipCode", th.StringType))),
        
        th.Property("Addressess_Home", th.PropertiesList(
            th.Property("City", th.StringType),
            th.Property("CountryId", th.PropertiesList(
                th.Property("Id", th.UUIDType),
                th.Property("Title", th.StringType))),

            th.Property("CreatedOnSystem", th.DateTimeType),
            th.Property("Id", th.UUIDType),
            th.Property("IsPrimary", th.BooleanType),
            th.Property("Line1", th.StringType),
            th.Property("Line2", th.StringType),
            th.Property("Location", th.PropertiesList(
                th.Property("Lat", th.NumberType),
                th.Property("Lon", th.NumberType))),

            th.Property("State", th.StringType),
            th.Property("ZipCode", th.StringType))),

        th.Property("Addresses_Other", th.PropertiesList(
            th.Property("City", th.StringType),
            th.Property("CountryId", th.PropertiesList(
                th.Property("Id", th.UUIDType),
                th.Property("Title", th.StringType))),

            th.Property("CreatedOnSystem", th.DateTimeType),
            th.Property("Id", th.UUIDType),
            th.Property("IsPrimary", th.BooleanType),
            th.Property("Line1", th.StringType),
            th.Property("Line2", th.StringType),
            th.Property("Location", th.PropertiesList(
                th.Property("Lat", th.NumberType),
                th.Property("Lon", th.NumberType))),

            th.Property("State", th.StringType),
            th.Property("ZipCode", th.StringType))),

        th.Property("ContactId", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("ContactSourceId", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("CreatedById", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("CreatedOn", th.DateTimeType),

        th.Property("CurrentCompanyId", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("Education", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("IsPrimary", th.BooleanType),
            th.Property("Value", th.StringType),
            th.Property("ValueIds", th.PropertiesList(
                th.Property("Id", th.UUIDType),
                th.Property("Title", th.StringType))),

            th.Property("WhatId", th.PropertiesList(
                th.Property("Id", th.UUIDType),
                th.Property("Title", th.StringType),
                th.Property("EnittyName", th.StringType))),

            th.Property("WhatValue", th.StringType),
            th.Property("WhenEnd", th.DateTimeType),
            th.Property("WhenStart", th.DateTimeType),
            th.Property("WhereId", th.PropertiesList(
                th.Property("Id", th.UUIDType),
                th.Property("Title", th.StringType),
                th.Property("EnittyName", th.StringType))))),

        th.Property("EmailAddresses_Other", th.PropertiesList(
            th.Property("CreatedOnSystem", th.DateTimeType),
            th.Property("Id", th.UUIDType),
            th.Property("IsPrimary", th.BooleanType),
            th.Property("Value", th.StringType))),

        th.Property("EmailAddresses_Personal", th.PropertiesList(
            th.Property("CreatedOnSystem", th.DateTimeType),
            th.Property("Id", th.UUIDType),
            th.Property("IsPrimary", th.BooleanType),
            th.Property("Value", th.StringType))),

        th.Property("EmailAddresses_Work", th.PropertiesList(
            th.Property("CreatedOnSystem", th.DateTimeType),
            th.Property("Id", th.UUIDType),
            th.Property("IsPrimary", th.BooleanType),
            th.Property("Value", th.StringType))),

        th.Property("ExternalPrimaryKey", th.StringType),
        th.Property("FirstName", th.StringType),
        th.Property("FormId", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("Id", th.UUIDType),
        th.Property("JobId", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("JobTitle", th.StringType),
        th.Property("LastName", th.StringType),
        th.Property("MiddleName", th.StringType),
        th.Property("ModifiedOn", th.DateTimeType),
        th.Property("Name", th.StringType),
        th.Property("NickName", th.StringType),
        th.Property("Owners", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType),
            th.Property("IsPrimary", th.BooleanType))),

        th.Property("ParsedCity", th.StringType),
        th.Property("ParsedEmail", th.StringType),
        th.Property("ParsedFirstName", th.StringType),
        th.Property("ParsedLastName", th.StringType),
        th.Property("ParsedPhone", th.StringType),
        th.Property("ParsedState", th.StringType),
        th.Property("ParsedZip", th.StringType),
        th.Property("PhoneNumbers_Fax", th.PropertiesList(
            th.Property("CreatedOnSystem", th.DateTimeType),
            th.Property("Extension", th.StringType),
            th.Property("Id", th.UUIDType),
            th.Property("IsPrimary", th.BooleanType),
            th.Property("PhoneFlagTypeId", th.PropertiesList(
                th.Property("Id", th.UUIDType),
                th.Property("Title", th.StringType))),

            th.Property("Value", th.StringType))),

        th.Property("PhoneNumbers_Home", th.PropertiesList(
            th.Property("CreatedOnSystem", th.DateTimeType),
            th.Property("Extension", th.StringType),
            th.Property("Id", th.UUIDType),
            th.Property("IsPrimary", th.BooleanType),
            th.Property("PhoneFlagTypeId", th.PropertiesList(
                th.Property("Id", th.UUIDType),
                th.Property("Title", th.StringType))),

            th.Property("Value", th.StringType))),

        th.Property("PhoneNumbers_Mobile", th.PropertiesList(
            th.Property("CreatedOnSystem", th.DateTimeType),
            th.Property("Extension", th.StringType),
            th.Property("Id", th.UUIDType),
            th.Property("IsPrimary", th.BooleanType),
            th.Property("PhoneFlagTypeId", th.PropertiesList(
                th.Property("Id", th.UUIDType),
                th.Property("Title", th.StringType))),

            th.Property("Value", th.StringType))),

        th.Property("PhoneNumbers_Other", th.PropertiesList(
            th.Property("CreatedOnSystem", th.DateTimeType),
            th.Property("Extension", th.StringType),
            th.Property("Id", th.UUIDType),
            th.Property("IsPrimary", th.BooleanType),
            th.Property("PhoneFlagTypeId", th.PropertiesList(
                th.Property("Id", th.UUIDType),
                th.Property("Title", th.StringType))),

            th.Property("Value", th.StringType))),

        th.Property("PhoneNumbers_Skype", th.PropertiesList(
            th.Property("CreatedOnSystem", th.DateTimeType),
            th.Property("Extension", th.StringType),
            th.Property("Id", th.UUIDType),
            th.Property("IsPrimary", th.BooleanType),
            th.Property("PhoneFlagTypeId", th.PropertiesList(
                th.Property("Id", th.UUIDType),
                th.Property("Title", th.StringType))),

            th.Property("Value", th.StringType))),

        th.Property("PhoneNumbers_Work_Direct", th.PropertiesList(
            th.Property("CreatedOnSystem", th.DateTimeType),
            th.Property("Extension", th.StringType),
            th.Property("Id", th.UUIDType),
            th.Property("IsPrimary", th.BooleanType),
            th.Property("PhoneFlagTypeId", th.PropertiesList(
                th.Property("Id", th.UUIDType),
                th.Property("Title", th.StringType))),

            th.Property("Value", th.StringType))),

        th.Property("PhoneNumbers_Work_Main", th.PropertiesList(
            th.Property("CreatedOnSystem", th.DateTimeType),
            th.Property("Extension", th.StringType),
            th.Property("Id", th.UUIDType),
            th.Property("IsPrimary", th.BooleanType),
            th.Property("PhoneFlagTypeId", th.PropertiesList(
                th.Property("Id", th.UUIDType),
                th.Property("Title", th.StringType))),

            th.Property("Value", th.StringType))),

        th.Property("RawHttpReferer", th.StringType),
        th.Property("SourceId", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("StatusId", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("Tags", th.PropertiesList(
            th.Property("property1", th.PropertiesList(
                th.Property("Id", th.UUIDType),
                th.Property("Title", th.StringType))),

            th.Property("property2", th.PropertiesList(
                th.Property("Id", th.UUIDType),
                th.Property("Title", th.StringType))))),

        th.Property("Websites_Blog", th.PropertiesList(
            th.Property("CreatedOnSystem", th.DateTimeType),
            th.Property("Id", th.UUIDType),
            th.Property("IsPrimary", th.BooleanType),
            th.Property("Value", th.StringType))),

        th.Property("Websites_Business", th.PropertiesList(
            th.Property("CreatedOnSystem", th.DateTimeType),
            th.Property("Id", th.UUIDType),
            th.Property("IsPrimary", th.BooleanType),
            th.Property("Value", th.StringType))),

        th.Property("Websites_Facebook", th.PropertiesList(
            th.Property("CreatedOnSystem", th.DateTimeType),
            th.Property("Id", th.UUIDType),
            th.Property("IsPrimary", th.BooleanType),
            th.Property("Value", th.StringType))),

        th.Property("Websites_LinkedIn", th.PropertiesList(
            th.Property("CreatedOnSystem", th.DateTimeType),
            th.Property("Id", th.UUIDType),
            th.Property("IsPrimary", th.BooleanType),
            th.Property("Value", th.StringType))),

        th.Property("Websites_Other", th.PropertiesList(
            th.Property("CreatedOnSystem", th.DateTimeType),
            th.Property("Id", th.UUIDType),
            th.Property("IsPrimary", th.BooleanType),
            th.Property("Value", th.StringType))),

        th.Property("Websites_Personal", th.PropertiesList(
            th.Property("CreatedOnSystem", th.DateTimeType),
            th.Property("Id", th.UUIDType),
            th.Property("IsPrimary", th.BooleanType),
            th.Property("Value", th.StringType))),
    ).to_dict()

class LossesStream(crelateStream):
    name = "losses"
    path = "/losses"
    #replication_key = "ModifiedOn"
    schema = th.PropertiesList(
        th.Property("CompetitorId", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("CreatedOn", th.DateTimeType),
        th.Property("CustomField1", th.StringType),
        th.Property("CustomField10", th.StringType),
        th.Property("CustomField11", th.NumberType),
        th.Property("CustomField12", th.NumberType),
        th.Property("CustomField13", th.NumberType),
        th.Property("CustomField14", th.NumberType),
        th.Property("CustomField15", th.NumberType),
        th.Property("CustomField16", th.NumberType),
        th.Property("CustomField17", th.NumberType),
        th.Property("CustomField18", th.NumberType),
        th.Property("CustomField19", th.NumberType),
        th.Property("CustomField2", th.StringType),
        th.Property("CustomField20", th.NumberType),
        th.Property("CustomField21", th.DateTimeType),
        th.Property("CustomField22", th.DateTimeType),
        th.Property("CustomField23", th.DateTimeType),
        th.Property("CustomField24", th.DateTimeType),
        th.Property("CustomField25", th.DateTimeType),
        th.Property("CustomField26", th.DateTimeType),
        th.Property("CustomField27", th.DateTimeType),
        th.Property("CustomField28", th.DateTimeType),
        th.Property("CustomField29", th.DateTimeType),
        th.Property("CustomField3", th.StringType),
        th.Property("CustomField30", th.DateTimeType),
        th.Property("CustomField31", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField32", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField33", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField34", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField35", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField36", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField37", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField38", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField39", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField4", th.StringType),
        th.Property("CustomField40", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField41", th.StringType),
        th.Property("CustomField42", th.StringType),
        th.Property("CustomField43", th.StringType),
        th.Property("CustomField44", th.StringType),
        th.Property("CustomField45", th.StringType),
        th.Property("CustomField46", th.StringType),
        th.Property("CustomField47", th.StringType),
        th.Property("CustomField48", th.StringType),
        th.Property("CustomField49", th.StringType),
        th.Property("CustomField5", th.NumberType),
        th.Property("CustomField50", th.StringType),
        th.Property("CustomField51", th.NumberType),
        th.Property("CustomField52", th.NumberType),
        th.Property("CustomField53", th.NumberType),
        th.Property("CustomField54", th.NumberType),
        th.Property("CustomField55", th.NumberType),
        th.Property("CustomField56", th.NumberType),
        th.Property("CustomField57", th.NumberType),
        th.Property("CustomField58", th.NumberType),
        th.Property("CustomField59", th.NumberType),
        th.Property("CustomField6", th.StringType),
        th.Property("CustomField60", th.NumberType),
        th.Property("CustomField61", th.DateTimeType),
        th.Property("CustomField62", th.DateTimeType),
        th.Property("CustomField63", th.DateTimeType),
        th.Property("CustomField64", th.DateTimeType),
        th.Property("CustomField65", th.DateTimeType),
        th.Property("CustomField66", th.DateTimeType),
        th.Property("CustomField67", th.DateTimeType),
        th.Property("CustomField68", th.DateTimeType),
        th.Property("CustomField69", th.DateTimeType),
        th.Property("CustomField7", th.StringType),
        th.Property("CustomField70", th.DateTimeType),
        th.Property("CustomField71", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField72", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField73", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField74", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField75", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField76", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField77", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField78", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField79", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),
        th.Property("CustomField8", th.StringType),
        th.Property("CustomField80", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("CustomField9", th.StringType),

        th.Property("Id", th.UUIDType),
        th.Property("JobId", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("LossReasonTypeId", th.PropertiesList(
            th.Property("Id", th.UUIDType),
            th.Property("Title", th.StringType))),

        th.Property("ModifiedOn", th.DateTimeType),
        th.Property("When", th.DateTimeType),
    ).to_dict()