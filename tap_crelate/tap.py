"""crelate tap class."""

from __future__ import annotations

from singer_sdk import Tap
from singer_sdk import typing as th  # JSON schema typing helpers

from tap_crelate import streams
streams.ActivitiesStream
streams.TasksStream
streams.UsersStream
streams.PaymentsStream
streams.InvoicesStream
streams.PlacementsStream
streams.ApplicationsStream
streams.LossesStream


class Tapcrelate(Tap):
    """crelate tap class."""

    name = "tap-crelate"

    config_jsonschema = th.PropertiesList(
        th.Property(
            "api_key",
            th.StringType,
            required=True,
            secret=True,  # Flag config as protected.
            description="The key to authenticate against the API service",
        ),
        th.Property(
            "start_date",
            th.DateTimeType,
            description="The earliest record date to sync",
        ),
        th.Property(
            "api_url",
            th.StringType,
            default="https://app.crelate.com/api3",
            description="The url for the API service",
        ),
        th.Property(
            "content-type",
            th.StringType,
            default="application/json",
        ),
    ).to_dict()

    def discover_streams(self) -> list[streams.crelateStream]:
        """Return a list of discovered streams.

        Returns:
            A list of discovered streams.
        """
        return [
            streams.ActivitiesStream(self),
            streams.TasksStream(self),
            streams.UsersStream(self),
            streams.PaymentsStream(self),
            streams.InvoicesStream(self),
            streams.PlacementsStream(self),
            streams.ApplicationsStream(self),
            streams.LossesStream(self),
        ]


if __name__ == "__main__":
    Tapcrelate.cli()
